function limpaFormulárioCep() {
    // Limpa valores do formulário de cep.
    $("#endereco").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
}
function buscaCep(){
    //Nova variável "cep" somente com dígitos.
    var cep = $('#cep').val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep == "" || cep.length < 8) {
        eModal.alert("CEP inválido");
        limpaFormulárioCep();
        return;
    }
    //Preenche os campos com "..." enquanto consulta webservice.
    $("#endereco").val("...");
    $("#bairro").val("...");
    $("#cidade").val("...");
    $("#uf").val("...");

    //Consulta o webservice viacep.com.br/
    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
        if (!("erro" in dados)) {
            $("#endereco").val(dados.logradouro);
            $("#bairro").val(dados.bairro);
            $("#cidade").val(dados.localidade);
            $("#uf").val(dados.uf);
        } //end if.
        else {
            limpaFormulárioCep();
            eModal.alert("CEP inválido");
        }
    });
}
