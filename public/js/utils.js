$(function() {
   $('.cpf').mask('000.000.000-00');
   $('.cep').mask('00000-000');
   $('.money2').mask("#.##0,00", {reverse: true});
   $('.number').mask('0#');
   $('.telefone').mask('(00)000000009');    
   $('.date').mask('00/00/0000');
    $("#textarea-maxlength").keyup(function(){
        $("#counter").text("Characters left: " + ($(this).attr('maxlength') - $(this).val().length));
    });

})

