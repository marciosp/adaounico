<?php

namespace App\Helpers;


class Format
{
    static public function removeMask($value) {
        $pattern = '/[\(\)\.\-]/';
        return preg_replace($pattern,'',$value);
    }
    /* Função para aplicar máscara em campos/valores como cpf, cnpj, data, hora, coordenadas estelares e o que desejar
     * Exemplo
     * $cnpj = "11222333000199";
     * $cpf = "00100200300";
     * $cep = "08665110";
     * $data = "10102010";
     * echo mask($cnpj,'##.###.###/####-##');
     * echo mask($cpf,'###.###.###-##');
     * echo mask($cep,'#####-###');
     * echo mask($data,'##/##/####');
     */
    public static function documentMask($val, $mask){
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++) {
            if($mask[$i] == '#') {
                if(isset($val[$k])) $maskared .= $val[$k++];
            } else {
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
            }
        }
        return $maskared;        
    }    
}
