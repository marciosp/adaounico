<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
class ProdutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            return [
                'nome' => 'required|unique:produto,nome,'.Request::input('id'),
                'categoria_id' => 'required|numeric',
                'quantidade' => 'required|numeric',
                'preco' => 'required|numeric'
            ];
    }

}
