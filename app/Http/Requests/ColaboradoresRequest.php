<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
class ColaboradoresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            return [
                'nome' => 'required|unique:colaboradores,nome,'.Request::input('id'),
                'cargo_id' => 'required|numeric',
                'telefone' => 'required',
                'cpf' => 'required|cpf',
                'rg' => 'required',
                'cep' => 'required',
                'endereco' => 'required',
                'numero' => 'required|numeric',
                'bairro' => 'required',
                'cidade' => 'required',
                'estado' => 'required'            
            ];
    }

}
