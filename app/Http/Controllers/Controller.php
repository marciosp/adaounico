<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Support\Facades\Input;
//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $status = ["<i class='fas fa-times-circle'></i>", "<i class='fas fa-check-circle'></i>"];
    protected $namespaceModel;
    protected $model;
    protected $request;
    protected $extraData = [];
    protected $validationRules = [];

    public function __construct(Request $request) {
        $this->model = $request->segments()[1];
        $this->namespaceModel = '\App\Models\\' . ucfirst($this->model);
        $this->request = $request;
    }
    public function index()
    {
      return response($this->namespaceModel::all()->jsonSerialize(), Response::HTTP_OK);
    }
    public function lista() {
        $namespaceModel = $this->namespaceModel;        
        $itens = $namespaceModel::paginate(10);        
        if ($this->request->input('search')) {            
            $itens = $namespaceModel::where('nome', 'like', '%' . $this->request->input('search') . '%')->paginate(10);
        }
        
        $dados = array_merge(['itens' => $itens, 'status' => $this->status], $this->extraData);        
        return view("admin.{$this->model}.listagem", $dados);
    }

    public function get($id) {
        $namespaceModel = $this->namespaceModel;
        $item = $namespaceModel::find($id);
        return view("admin.{$this->model}.detalhe", ['item' => $item, 'status' => $this->status]);
    }

    public function form($id = null) {
        $item = null;
        if ($id) {
            $namespaceModel = $this->namespaceModel;
            $item = $namespaceModel::find($id);
        }
        return view("admin.{$this->model}.formulario", ['item' => $item]);
    }

    public function salvar($input = null) {
        $namespaceModel = $this->namespaceModel;
        $input = empty($input) ? $this->request->all() : $input;
        $validator = Validator::make($input, $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        empty($input['id']) ? $namespaceModel::create($input) : $namespaceModel::find($input['id'])->update($input);
        return back()->with(['status' => 'success', 'msg' => $input['id'] ? "Editado com sucesso" : "Cadastrado com sucesso"]);
    }

    public function delete($id) {
        $namespaceModel = $this->namespaceModel;
        $namespaceModel::destroy($id);
        return back()->with(['status' => 'success', 'msg' => 'Apagado com sucesso']);
    }
}
