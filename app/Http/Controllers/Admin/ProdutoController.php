<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Produto;
use App\Models\Categoria;
use App\Http\Requests\ProdutoRequest;
use Response;
use DB;

class ProdutoController extends Controller
{
    public function lista() {
        $produtos = Produto::paginate(10);
        if($this->request->input('search')) {
            $produtos = Produto::where('nome', 'LIKE', '%' . $this->request->input('search') . '%')->paginate(10);
        }
        return view('admin.estoque.produto.listagem',['produtos'=>$produtos]);        
    }
    
    public function get($id) {
        $produto = Produto::find($id);
        return view('admin.estoque.produto.detalhe',['p'=>$produto]);        
      
    }
    public function form($id = null) {
        $categorias = Categoria::lists('categoria','id');
//        echo "<pre>"; print_r($categorias->); exit;
//        Category::lists('name', 'id');
        $produto = null;
        if($id) {
            $produto = Produto::find($id);
        }
        
        return view('admin.estoque.produto.formulario',['p' => $produto,'cs' => $categorias]);        
    }
    public function save(ProdutoRequest $request) {
        $input = $request->all();
        $produto = empty($input['id']) ? Produto::create($input) : Produto::find($input['id'])->update($input);        
        return back()->with(['status' => 'success','msg'=>'Cadastrado com sucesso']);
    }

    public function delete($id) {
        Produto::destroy($id);
        return back()->with(['status' => 'success','msg'=>'Apagado com sucesso']);
    }
}
