<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CursosController extends Controller

{
    public function __construct(Request $request) {
        $this->validationRules = [
                'curso' => 'required|unique:cursos',
        ];
        parent::__construct($request);
    }
}