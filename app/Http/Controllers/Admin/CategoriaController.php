<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categoria;
use App\Http\Requests\CategoriaRequest;
use Response;
use Validator;
use DB;

class CategoriaController extends Controller
{
    public function lista() {
        $categorias = DB::table('categoria');
        if($this->request->input('search')) {
             $categorias->where('categoria', 'LIKE', '%' . $this->request->input('search') . '%');
        }
        $categorias = $categorias->whereNull('deleted_at')->paginate(10);
        return view('admin.estoque.categoria.listagem',['categorias'=>$categorias, 'status' => $this->status ]);        
    }
    
    public function get($id) {
        $categoria = Categoria::find($id);
        return view('admin.estoque.categoria.detalhe',['c'=>$categoria]);        
      
    }
    public function form($id = null) {
        
        $categoria = null;
        if($id) {
            $categoria = Categoria::find($id);
        }
        
        return view('admin.estoque.categoria.formulario',['categoria' => $categoria]);        
    }
    public function save(CategoriaRequest $request) {
        $input = $request->all();
        $categoria = empty($input['id']) ? Categoria::create($input) : Categoria::find($input['id'])->update($input);
        
        return back()->with(['status' => 'success','msg'=>'Cadastrado com sucesso']);
    }

    public function delete($id) {
        Categoria::destroy($id);
        return back()->with(['status' => 'success','msg'=>'Apagado com sucesso']);
    }
}
