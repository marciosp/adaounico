<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Colaboradores;
use App\Models\Cargos;
use App\Models\Bancos;
use App\Models\Estados;
use App\Http\Requests\ColaboradoresRequest;

class ColaboradoresController extends Controller
{
    public function lista() {
        $colaboradores = Colaboradores::paginate(10);
        if($this->request->input('search')) {
            $colaboradores = Colaboradores::where('nome', 'LIKE', '%' . $this->request->input('search') . '%')->paginate(10);
        }
        return view('admin.colaboradores.listagem',['colaboradores'=>$colaboradores, 'status' => $this->status]);        
    }
    
    public function get($id) {
        $colaborador = Colaboradores::find($id);
        return view('admin.colaboradores.detalhe',['c'=>$colaborador,'status'=>$this->status,'estados'=>Estados::$estadosBrasileiros]);        
      
    }
    public function form($id = null) {
        $cargos = Cargos::pluck('cargo','id');
//        $cargos = [];
        $bancos = Bancos::$bancos;        
        $estados = Estados::$estadosBrasileiros;
        $bancos = array_reduce(array_map(function ($item) {return [$item['name'] => $item['name']];},$bancos), 'array_merge', array());
        $colaborador = null;
        if($id) {
            $colaborador = Colaboradores::find($id);
        }
        
        return view('admin.colaboradores.formulario',['c' => $colaborador,'cs' => $cargos, 'bcs' => $bancos,'uf'=>$estados]);        
    }
    public function save(ColaboradoresRequest $request) {
        $input = $request->all();
        $input['telefone'] = Format::removeMask($input['telefone']);
        $input['cpf'] = Format::removeMask($input['cpf']);
        $input['rg'] = Format::removeMask($input['rg']);
        $input['cep'] = Format::removeMask($input['cep']);

        empty($input['id']) ? Colaboradores::create($input) : Colaboradores::find($input['id'])->update($input);        
        return back()->with(['status' => 'success','msg'=> $input['id'] ? "Editado com sucesso" : "Cadastrado com sucesso"]);
    }

    public function delete($id) {
        Colaboradores::destroy($id);
        return back()->with(['status' => 'success','msg'=>'Apagado com sucesso']);
    }
}