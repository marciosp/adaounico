<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Convenio;
use App\Http\Requests\ConvenioRequest;

class ConvenioController extends Controller
{
//    public function __construct() {   
//        $this->validationRules = [
//                'nome' => 'required|unique:convenio,nome,'.@$_POST['id'],
//            ];
//        parent::__construct();
//    }
    public function __construct(Request $request) {
        $this->validationRules = [
                'nome' => 'required|unique:convenio,nome,'.$request->input('id'),
            ];
        
        parent::__construct($request);
    }
}