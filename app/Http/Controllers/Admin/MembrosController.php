<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Membros;
use App\Models\Cargos;
use App\Models\Cursos;
use App\Models\Funcoes;
use App\Models\Bancos;
use App\Models\Estados;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use App\Helpers\Format;

class MembrosController extends Controller
{
    public $status = ['0' => 'Inativo', '1' => 'Ativo'];
    public $bool = ['0' => 'Não', '1' => 'Sim'];
    public function __construct(Request $request) {
       $this->validationRules = [
//               'nome' => 'required|unique:membros',
//                 'input_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
               'nome' => 'required',           
           
       ];
       
       $this->middleware('before');
       parent::__construct($request);
    }
    public function lista() {
        $namespaceModel = $this->namespaceModel;           
        $itens = $namespaceModel::paginate(12);   
        if ($this->request->input('search')) {            
            $itens = $namespaceModel::where('nome', 'like', '%' . $this->request->input('search') . '%')->paginate(10);
        }
        $this->request->session()->put('page', $itens->lastPage());  
        $dados = ['itens' => $itens, 'status' => $this->status,'total' => $itens->total()];
        return view("admin.{$this->model}.listagem", $dados);
    }    
    public function form($id = null) {
        $cargos = Cargos::pluck('cargo','id');
        $cursos = Cursos::pluck('curso','id');
        $funcoes = Funcoes::pluck('funcao','id');
        $estados = Estados::$estadosBrasileiros;
        
        $membros = null;        
        if($id) {
            $membros = Membros::find($id);            
            $this->request->session()->put('url', url()->previous());                
        }        
        return view('admin.membros.formulario',[
            'm' => $membros,
            'cs' => $cargos, 
            'fs' => $funcoes, 
            'crs' => $cursos, 
            'uf'=>$estados
            ]);        
    }    
    public function salvar($input = null) {
        $input = empty($input) ? $this->request->all() : $input;
        $input['telefone'] = @Format::removeMask($input['telefone']);
        $input['cpf'] = @Format::removeMask($input['cpf']);
        $input['cep'] = @Format::removeMask($input['cep']);
        $input['celular'] = @Format::removeMask($input['celular']);
        $input['dtnasc'] = empty($input['dtnasc']) ? NULL : date('Y-m-d', strtotime(@$input['dtnasc']));
        $input['dt_batismo'] = empty($input['dt_batismo']) ? NULL : date('Y-m-d', strtotime(@$input['dt_batismo']));        
        $input['dt_consagrado'] = empty($input['dt_consagrado']) ? NULL : date('Y-m-d', strtotime(@$input['dt_consagrado']));
        $input['tempo_fe'] = @$input['tempo_fe']['ano'] * 12 + @$input['tempo_fe']['mes'];
        $input['dt_filiacao'] = empty($input['dt_filiacao']) ? NULL : date('Y-m-d', strtotime(@$input['dt_filiacao']));
       if($this->request->file('foto')) {
            $file = $this->request->file('foto');
            $input['foto'] = $this->request->foto->hashName();
        }
        $validator = Validator::make($input, $this->validationRules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        if(empty($input['id'])) {
            $membro = Membros::create($input);            
            if(!empty($input['filho'][0]['nome'])) {
                $membro->filhos()->createMany($input['filho']);
            }
        } else {
            $membro = Membros::find($input['id']);
            
            $fotoAntiga = $membro->foto;
            $membro->update($input);
            $membro->filhos()->delete();
            if(!empty($input['filho'][0]['nome'])) {
                $membro->filhos()->createMany($input['filho']);
            }
        }
        if ($this->request->hasFile('foto')) {
            $image = $this->request->file('foto');
            $name = $input['foto'];
            $destinationPath = public_path("/upload/membros/".$membro->id);
            @unlink(public_path("/upload/membros/".$membro->id."/".$fotoAntiga));    
            $image->move($destinationPath, $name);
        }
        
        $url = $input['id'] ? $this->request->session()->pull('url') : "/admin/membros/lista?page=".$this->request->session()->get('page');
        return redirect($url)->with(['status' => 'success','msg'=> $input['id'] ? "Editado com sucesso" : "Cadastrado com sucesso"]);
    }
    public function get($id) {
        $membros = Membros::find($id);
        $filhos = $membros->filhos()->get();
        return view('admin.membros.detalhe',['m'=>$membros,'filhos' => $filhos, 'status'=>$this->status,'estados'=>Estados::$estadosBrasileiros, 'bool' => $this->bool]);
    }
}