<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CargosController extends Controller

{
    public function __construct(Request $request) {
        $this->validationRules = [
                'cargo' => 'required|unique:cargos',
        ];
        parent::__construct($request);
    }
}