<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FuncoesController extends Controller

{
    public function __construct(Request $request) {
        $this->validationRules = [
                'funcao' => 'required|unique:funcoes',
        ];
        parent::__construct($request);
    }
}
