<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Controller{

    protected $namespaceModel;
    protected $model;
    protected $request;
    protected $extraData = [];
    protected $validationRules = [];

    public function __construct(Request $request) {
        $this->model = $request->segments()[1];
        $this->namespaceModel = '\App\Models\\' . ucfirst($this->model);
        $this->request = $request;
    }
    public function index()
    {

        $response = $this->namespaceModel::with('filhos','cursos','cargos','funcoes')->get();
        return response()->json($response,Response::HTTP_OK,[],JSON_PRETTY_PRINT);
    }
    
    public function show($id)
    {
        $response = $this->namespaceModel::with('filhos','cursos','cargos','funcoes')->find($id);
        return response()->json($response,Response::HTTP_OK,[],JSON_PRETTY_PRINT);
    }    
    public function create()
    {
      return response($this->namespaceModel::all()->jsonSerialize(), Response::HTTP_OK);
    }
    public function update()
    {
      return response($this->namespaceModel::all()->jsonSerialize(), Response::HTTP_OK);
    }
    public function destroy()
    {
      return response($this->namespaceModel::all()->jsonSerialize(), Response::HTTP_OK);
    }
}
