<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Categoria;

class Produto extends Model
{
    use SoftDeletes;
    protected $table = 'produto';
    protected $fillable = ['nome','descricao','quantidade','preco','categoria_id'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $guarded = ['id'];
    public function categoria() {
        return $this->belongsTo('App\Models\Categoria','categoria_id','id')
        // ->withDefault(
        //     ['nome' => 'Guest Author']
        // )
        ;
    }    
}