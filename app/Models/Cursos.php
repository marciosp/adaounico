<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cursos extends Model
{
    use SoftDeletes;
    protected $table = 'cursos';
    protected $fillable = ['curso'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $search = 'curso';
   
}
