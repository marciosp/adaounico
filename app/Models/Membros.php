<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Membros extends Model
{
    use SoftDeletes;
    protected $table = 'membros';
    protected $fillable = [
        'nome','cpf','rg','dtnasc','naturalidade','email','profissao','nome_pai','nome_mae','estado_civil','escolaridade','telefone','celular',
        'cep','endereco','bairro','cidade','estado','status','batismo','dt_batismo','batismo_espirito','tempo_fe','funcoes_id','cursos_id',
        'cargos_id','dt_consagrado','local_consagrado','foto','dt_filiacao','numero'
        ];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $search = 'nome';
    public function filhos() {
        return $this->hasMany('App\Models\Filhos');
    }
    public function cargos() {
        return $this->hasOne('App\Models\Cargos','id','cargos_id');
    }    
    public function funcoes() {
        return $this->hasOne('App\Models\Funcoes','id','funcoes_id');
    }    
        
    public function cursos() {
        return $this->hasOne('App\Models\Cursos','id','cursos_id');
    }    
    public function getDtnascAttribute($value)
    {
        return empty($value) || $value == '1970-01-01' ? '' : \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
    public function getDtfiliacaoAttribute($value)
    {
        return empty($value) || $value == '1970-01-01' ? '' : \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
    public function getDtbatismoAttribute($value)
    {
        return empty($value) || $value == '1970-01-01' ? '' : \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
    public function getDtconsagradoAttribute($value)
    {
        return empty($value) || $value == '1970-01-01' ? '' : \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
}