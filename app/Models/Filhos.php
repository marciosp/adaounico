<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Filhos extends Model
{
//    use SoftDeletes;
    protected $table = 'filhos';
    protected $fillable = ['nome','dtnasc'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $primaryKey = 'id';
    public function setDtnascAttribute($value)
    {
        $this->attributes['dtnasc'] = empty($value) ? NULL : date('Y-m-d', strtotime(@$value));
    }
    public function getDtnascAttribute($value)
    {
        return empty($value) || $value == '1970-01-01' ? '' : \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
}
