<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Cargos;

class Colaboradores extends Model
{
    use SoftDeletes;
    protected $table = 'colaboradores';
    protected $fillable = ['cargo_id','nome','telefone','cpf','rg','cep','endereco','numero','complemento','bairro','cidade','estado','status','banco','agencia','conta','operacao','observacao'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $guarded = ['id'];
    public function cargos() {
        return $this->belongsTo('App\Models\Cargos','cargo_id','id');
    }    
}