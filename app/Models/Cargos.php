<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cargos extends Model
{
    use SoftDeletes;
    protected $table = 'cargos';
    protected $fillable = ['cargo'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $search = 'cargo';
}
