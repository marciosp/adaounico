<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Funcoes extends Model
{
    use SoftDeletes;
    protected $table = 'funcoes';
    protected $fillable = ['funcao'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $search = 'funcao';
}
