<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Convenio extends Model
{
    use SoftDeletes;
    protected $table = 'convenio';
    protected $fillable = ['nome','status'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $search = 'nome'; //coluna a ser buscada por padrao
}
