<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;
    protected $table = 'categoria';
    protected $fillable = ['categoria'];
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    protected $guarded = ['id'];
}
