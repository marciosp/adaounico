<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Verb Path Action Route Name
//GET /users index users.index
//GET /users/create create users.create
//POST /users store users.store
//GET /users/{user} show users.show
//GET /users/{user}/edit edit users.edit
//PUT|PATCH /users/{user} update users.update
//DELETE /users/{user} destroy users.destroy

Route::resource('membros', 'Api\Controller');