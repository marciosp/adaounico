<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$alias = [
    'lista' => 'lista',
    'save' => 'save',
    'remove' => 'delete',
    'mostra' => 'id'
];
//Route::get('/', function () {
//    return redirect('/login');
//});
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
//    'middleware' => 'auth'
], function () {
    Route::get('/home', 'HomeController@index');    
//    Route::match(['get', 'post'],'/categorias','CategoriaController@lista');
//    Route::get('/categoria/{id?}','CategoriaController@form');     
//    Route::post('/categoria/save/{id?}','CategoriaController@save');
//    Route::get('/categoria/remove/{id}','CategoriaController@delete');
//    Route::get('/categoria/mostra/{id}','CategoriaController@get');
//    Route::match(['get', 'post'],'/produtos','ProdutoController@lista');
//    Route::get('/produto/{id?}','ProdutoController@form');
//    Route::post('/produto/save/{id?}','ProdutoController@save');
//    Route::get('/produto/remove/{id}','ProdutoController@delete');
//    Route::get('/produto/mostra/{id}','ProdutoController@get');    
//    Route::match(['get', 'post'],'/funcoes','FuncoesController@lista');
//    Route::get('/cargo/{id?}','CargosController@form');
//    Route::post('/cargo/save/{id?}','CargosController@save');
//    Route::get('/cargo/remove/{id}','CargosController@delete');
//    Route::get('/cargo/mostra/{id}','CargosController@get');    
//    Route::match(['get', 'post'],'/colaboradores','ColaboradoresController@lista');
//    Route::get('/colaborador/{id?}','ColaboradoresController@form');
//    Route::post('/colaborador/save/{id?}','ColaboradoresController@save');
//    Route::get('/colaborador/remove/{id}','ColaboradoresController@delete');
//    Route::get('/colaborador/mostra/{id}','ColaboradoresController@get');    
    Route::match(['get', 'post'],'{model}/{controllerMethod}/{id?}',function($model, $method, $id = null) {
        $alias = [    
            'lista' => 'lista',
            'salvar' => 'salvar',
            'remove' => 'delete',
            'mostra' => 'get',
            'editar' => 'form',
            'cadastrar' => 'form'
            ]; 
        $app = app();
        $controller = $app->make("App\Http\Controllers\Admin\\".ucfirst($model)."Controller");
        return $controller->callAction($alias[$method], $parameters = [$id]);
    });
});

Route::auth();
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('index');
});

