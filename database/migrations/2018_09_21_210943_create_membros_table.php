<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembrosTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('membros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('cpf',14)->nullable();
            $table->string('rg',14)->nullable();
            $table->date('dtnasc')->nullable();
            $table->string('naturalidade',64)->nullable();
            $table->string('email')->nullable();
            $table->string('profissao')->nullable();
            $table->string('nome_pai')->nullable();
            $table->string('nome_mae')->nullable();
            $table->string('estado_civil',12)->nullable();
            $table->string('escolaridade',20)->nullable();
            $table->string('telefone',12)->nullable();
            $table->string('celular',12)->nullable();
            $table->string('cep',10)->nullable();
            $table->string('endereco')->nullable();
            $table->integer('numero')->nullable();
            $table->string('bairro',64)->nullable();
            $table->string('cidade',64)->nullable();
            $table->string('estado',2)->nullable();
            $table->boolean('status')->nullable()->default(1); 
            $table->boolean('batismo')->nullable();
            $table->date('dt_batismo')->nullable();
            $table->date('dt_filiacao')->nullable();
            $table->boolean('batismo_espirito')->nullable();
            $table->integer('tempo_fe')->nullable()->comment = 'Valor em meses';
            $table->integer('funcoes_id')->nullable();
            $table->integer('cursos_id')->nullable();
            $table->integer('cargos_id')->nullable();
            $table->date('dt_consagrado')->nullable();
            $table->string('local_consagrado')->nullable();
            $table->string('foto',128)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membros');
    }
}
