<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CargosDatabaseSeeder::class);
        $this->call(CursosDatabaseSeeder::class);
        $this->call(FuncoesDatabaseSeeder::class);
        $this->call(MembrosDatabaseSeeder::class);
        $this->call(FilhosDatabaseSeeder::class);
        $this->call(PasswordResetsDatabaseSeeder::class);
        $this->call(UsersDatabaseSeeder::class);
    }
}
