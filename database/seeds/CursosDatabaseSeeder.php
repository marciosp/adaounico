<?php

use Illuminate\Database\Seeder;
use Elimuswift\DbExporter\SeederHelper;

class CursosDatabaseSeeder extends Seeder 
{

	use SeederHelper;
	
    public function run()
    {
        
        $data = [
            
            [
                'id' => '1',
                'curso' => 'Basico',
                'created_at' => '2018-09-27 21:16:33',
                'updated_at' => '2018-09-27 21:16:33',
                'deleted_at' => null,
            ],

            [
                'id' => '2',
                'curso' => 'Medio',
                'created_at' => '2018-09-27 21:16:37',
                'updated_at' => '2018-09-27 21:16:37',
                'deleted_at' => null,
            ],

            [
                'id' => '3',
                'curso' => 'Avançado',
                'created_at' => '2018-09-27 21:16:42',
                'updated_at' => '2018-09-27 21:16:42',
                'deleted_at' => null,
            ],

        ];

        foreach($data as $item) 
        {
            $this->saveData("cursos", $item);
        }
    }

}