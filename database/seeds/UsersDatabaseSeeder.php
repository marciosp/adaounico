<?php

use Illuminate\Database\Seeder;
use Elimuswift\DbExporter\SeederHelper;

class UsersDatabaseSeeder extends Seeder 
{

	use SeederHelper;
	
    public function run()
    {
        
        $data = [
            
            [
                'id' => '1',
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => null,
                'password' => '$2y$10$eKW7qh4FyfcqX9DaaVx5vOsW6Zw50qd.uid9fkXhAmDf6eexsgKky',
                'remember_token' => null,
                'created_at' => '2018-09-27 22:23:58',
                'updated_at' => '2018-09-27 22:23:58',
                'deleted_at' => null,
            ]

        ];

        foreach($data as $item) 
        {
            $this->saveData("users", $item);
        }
    }

}