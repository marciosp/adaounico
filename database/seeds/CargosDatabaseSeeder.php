<?php

use Illuminate\Database\Seeder;
use Elimuswift\DbExporter\SeederHelper;

class CargosDatabaseSeeder extends Seeder 
{

	use SeederHelper;
	
    public function run()
    {
        
        $data = [
            
            [
                'id' => '1',
                'cargo' => 'Pastor',
                'created_at' => '2018-09-27 21:16:07',
                'updated_at' => '2018-09-27 21:16:07',
                'deleted_at' => null,
            ],

            [
                'id' => '2',
                'cargo' => 'Diacono',
                'created_at' => '2018-09-27 21:16:10',
                'updated_at' => '2018-09-27 21:16:10',
                'deleted_at' => null,
            ],

            [
                'id' => '3',
                'cargo' => 'Presbitero',
                'created_at' => '2018-09-27 21:16:20',
                'updated_at' => '2018-09-27 21:16:20',
                'deleted_at' => null,
            ],

        ];

        foreach($data as $item) 
        {
            $this->saveData("cargos", $item);
        }
    }

}