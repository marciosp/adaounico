<?php

use Illuminate\Database\Seeder;
use Elimuswift\DbExporter\SeederHelper;

class FilhosDatabaseSeeder extends Seeder 
{

	use SeederHelper;
	
    public function run()
    {
        
        $data = [
            
            [
                'id' => '1',
                'funcao' => 'Obreiro',
                'created_at' => '2018-09-27 21:12:08',
                'updated_at' => '2018-09-27 21:12:08',
                'deleted_at' => null,
            ],

            [
                'id' => '2',
                'funcao' => 'Tesoureiro',
                'created_at' => '2018-09-27 21:12:16',
                'updated_at' => '2018-09-27 21:12:16',
                'deleted_at' => null,
            ],

            [
                'id' => '3',
                'funcao' => 'Secretario',
                'created_at' => '2018-09-27 21:12:22',
                'updated_at' => '2018-09-27 21:12:22',
                'deleted_at' => null,
            ],

        ];

        foreach($data as $item) 
        {
            $this->saveData("funcoes", $item);
        }
    }

}