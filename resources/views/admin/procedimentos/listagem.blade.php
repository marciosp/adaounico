@extends('layouts.app')
@section('content')

<div class="container convenios">
    @if(empty($itens))
    <div class="alert alert-danger">
        Você não tem nenhum convenio cadastrado.
    </div>

    @else
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h2>Listagem de Convênios</h2>
        </div>
        <div class="col-md-6 col-xs-12">
            @include('admin.search')
        </div>
    </div>
    <div class="row text-right">
        <div class="col-md-12 margin-bottom">
            <a class="btn btn-default" href="cadastrar" role="button">Cadastrar</a>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12 col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>Nome</th>                        
                        <th>Status</th>

                        <th colspan="3" class="text-center">Ações</th>
                    </tr>
                    @foreach ($itens as $item)    
                    
                    <tr>
                        <td> {{$item->nome}} </td>
                        <td class="text-center"> {!! $status[$item->status] !!} </td>
                        <td class="text-center">
                            <a href="editar/{{$item->id}}">
                                <i class="far fa-edit"></i>
                            </a>
                        </td>        
                        <td class="text-center">
                            <a href="remove/{{$item->id}}" onclick="return confirm('Deseja apagar?')">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach 
                </table>

            </div>
        </div>
    </div>
</div>
<div class="row text-center">
    {{ $itens->links() }}
</div>

@endif
</div>
@endsection
