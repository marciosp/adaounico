@extends('layouts.app')
@section('content')
<div class="funcoes container">
    <h2>{{ $item ? 'Editar': 'Cadastrar' }} Funcão</h2>
    <div class="row text-right">
    <a class="btn btn-default" href="{{ $item ? '../': '' }}lista" role="button">Lista Completa</a>
   </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel-group">
                <div class=" panel-default">
                    <div class="panel-body">   
                        <form action="{{ $item ? '../': '' }}salvar" method="post" class="form-horizontal" role="form" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{@$item->id}}"/>
                                
                            <div class="form-group{{ $errors->has('funcao') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Função</label>
                                <div class="col-md-8">
                                    <input id="funcao" type="text" class="form-control" name="funcao" value="{{@$item->funcao ? $item->funcao : old('funcao')}}"  required autofocus/>
                                </div>
                                @if ($errors->has('funcao'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('funcao') }}</strong>
                                </span>
                                @endif
                            </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">
                       {{ $item ? 'Editar': 'Cadastrar' }}
                    </button>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection