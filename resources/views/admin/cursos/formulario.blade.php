@extends('layouts.app')
@section('content')
<div class="categorias container">
    <h2>{{ $item ? 'Editar': 'Cadastrar' }} Cursos</h2>
    <div class="row text-right">
    <a class="btn btn-default" href="{{ $item ? '../': '' }}lista" role="button">Lista Completa</a>
   </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel-group">
                <div class=" panel-default">
                    <div class="panel-body">   
                        <form action="{{ $item ? '../': '' }}salvar" method="post" class="form-horizontal" role="form" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{@$item->id}}"/>
                                
                            <div class="form-group{{ $errors->has('curso') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Curso</label>
                                <div class="col-md-8">
                                    <input id="curso" type="text" class="form-control" name="curso" value="{{@$item->curso ? $item->curso : old('curso')}}"  required autofocus/>
                                </div>
                                @if ($errors->has('curso'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('curso') }}</strong>
                                </span>
                                @endif
                            </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">
                       {{ $item ? 'Editar': 'Cadastrar' }}
                    </button>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection