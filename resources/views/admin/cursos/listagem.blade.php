@extends('layouts.app')

@section('content')
<section class="container">

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h2>Cursos</h2>
        </div>
        <div class="col-md-6 col-xs-12">           
        </div>
    </div>
    
    
    <div class="row text-right">
        <div class="col-md-12 margin-bottom">
            <a class="btn btn-default" href="cadastrar" role="button">Cadastrar</a>
        </div>
    </div>
    @if(count($itens) == 0)
    <div class="alert alert-danger">
        Você não tem nenhum curso cadastrado.
    </div>

    @else    
    <div class="row">

        <div class="col-md-6 col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>Curso</th>
                        <th colspan="2" class="text-center">Ações</th>
                    </tr>                    
                    @foreach ($itens as $c)       
                    <tr>
                        <td> {{$c->curso}} </td>
                        <td class="text-center">
                            <a href="editar/{{$c->id}}">
                                <i class="far fa-edit"></i>
                            </a>
                        </td>        
                        <td class="text-center">
                            <a href="remove/{{$c->id}}" onclick="return confirm('Deseja apagar?')">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach 
                </table>

            </div>
        </div>
    </div>
    <div class="row text-center">
        {{ $itens->links() }}
    </div>
    @endif
</div>


</section>
@endsection
