@extends('layouts.app')
@section('content')
<div class="container" id="colaboradores">
    <h2>{{ $c ?'Editar': 'Cadastrar' }} Colaborador</h2>
    <div class="row text-right">
        <a class="btn btn-default" href="/admin/colaboradores" role="button">Lista Completa</a>
    </div>


    <form action="/admin/colaborador/save" method="post" class="form-horizontal" role="form" novalidate>
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{@$c->id}}"/>


        <div>
            <h4>Dados Pessoais</h4>
        </div>
        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
            <label class="col-md-2 control-label">Nome*</label>
            <div class="col-md-10">
                <input id="nome" type="text" class="form-control" name="nome" value="{{@$c->nome ? $c->nome : old('nome')}}"  required autofocus/>

                @if ($errors->has('nome'))
                <span class="help-block">
                    <strong>{{ $errors->first('nome') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('telefone') ? 'has-error' : '' }}">
                {{Form::label('telefone', 'Telefone*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{Form::text('telefone', @$c->telefone ? $c->telefone : old('telefone'), ['class' => 'form-control telefone'])}}

                    @if ($errors->has('telefone'))
                    <div class="help-block">
                        <strong>{{ $errors->first('telefone') }}</strong>
                    </div>
                    @endif
                </div>
            </div>


            <div class="{{ $errors->has('cpf') ? 'has-error' : '' }}">
                {{Form::label('cpf', 'CPF*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{Form::text('cpf', @$c->cpf ? $c->cpf : old('cpf'), ['class' => 'form-control cpf'])}}
                    @if ($errors->has('cpf'))
                    <div class="help-block">
                        <strong>{{ $errors->first('cpf') }}</strong>
                    </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="form-group">

            <div class="{{ $errors->has('rg') ? 'has-error' : '' }}">
                {{Form::label('rg', 'RG*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{Form::text('rg', @$c->rg ? $c->rg : old('rg'), ['class' => 'form-control'])}}

                    @if ($errors->has('rg'))
                    <div class="help-block">
                        <strong>{{ $errors->first('rg') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="{{ $errors->has('cargo') ? 'has-error' : '' }}">
                {{Form::label('cargo', 'Cargo*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">      
                    {{ Form::select('cargo_id', $cs, @$c->cargo_id ? $c->cargo_id : old('cargo_id') , ['id' => 'cargo_id', 'class'=>'form-control']) }}
                   @if ($errors->has('cargo'))
                    <div class="help-block">
                        <strong>{{ $errors->first('cargo') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div>
            <h4>Endereco</h4>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('cep') ? 'has-error' : '' }}">
                {{Form::label('cep', 'CEP*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{Form::text('cep', @$c->cep ? $c->cep : old('cep'), ['class' => 'form-control cep'])}}
                    @if ($errors->has('cep'))
                    <div class="help-block">
                        <strong>{{ $errors->first('cep') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <button type="button" class="btn btn-default btn-xs cep-busca" onclick="buscaCep()">Preencher</button>
            </div>
        </div>
        <div class="form-group">

            <div class="{{ $errors->has('endereco') ? 'has-error' : '' }}">
                {{Form::label('endereco', 'Endereco*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-7">                                    
                    {{Form::text('endereco', @$c->endereco ? $c->endereco : old('endereco'), ['class' => 'form-control','id'=>'endereco'])}}

                    @if ($errors->has('endereco'))
                    <div class="help-block">
                        <strong>{{ $errors->first('endereco') }}</strong>
                    </div>
                    @endif
                </div>
            </div>


            <div class="{{ $errors->has('numero') ? 'has-error' : '' }}">
                {{Form::label('numero', 'Numero*', ['class' => 'col-md-1 control-label']) }}
                <div class="col-md-2">                                    
                    {{Form::text('numero', @$c->numero ? $c->numero : old('numero'), ['class' => 'form-control number','id' => 'numero'])}}
                    @if ($errors->has('numero'))
                    <div class="help-block">
                        <strong>{{ $errors->first('numero') }}</strong>
                    </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="form-group">

            <div class="{{ $errors->has('complemento') ? 'has-error' : '' }}">
                {{Form::label('complemento', 'Complemento', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{Form::text('complemento', @$c->complemento ? $c->complemento : old('complemento'), ['class' => 'form-control','id' => 'complemento'])}}

                    @if ($errors->has('complemento'))
                    <div class="help-block">
                        <strong>{{ $errors->first('complemento') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="{{ $errors->has('bairro') ? 'has-error' : '' }}">
                {{Form::label('bairro', 'Bairro*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{Form::text('bairro', @$c->bairro ? $c->bairro : old('bairro'), ['class' => 'form-control','id'=>'bairro'])}}
                    @if ($errors->has('bairro'))
                    <div class="help-block">
                        <strong>{{ $errors->first('bairro') }}</strong>
                    </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="form-group">
            <div class="{{ $errors->has('cidade') ? 'has-error' : '' }}">
                {{Form::label('cidade', 'Cidade*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{Form::text('cidade', @$c->cidade ? $c->cidade : old('cidade'), ['class' => 'form-control','id'=>'cidade'])}}

                    @if ($errors->has('cidade'))
                    <div class="help-block">
                        <strong>{{ $errors->first('cidade') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="{{ $errors->has('estado') ? 'has-error' : '' }}">
                {{Form::label('estado', 'Estado*', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{ Form::select('estado', array_merge(['' => 'Selecione o estado'],$uf), @$c->estado ? $c->estado : old('estado') , ['id' => 'uf', 'class'=>'form-control']) }}  
                    @if ($errors->has('estado'))
                    <div class="help-block">
                        <strong>{{ $errors->first('estado') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div>
            <h4>Detalhes Bancários</h4>
        </div>       
        <div class="form-group">
            <div class="{{ $errors->has('banco') ? 'has-error' : '' }}">
                {{Form::label('banco', 'Banco', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-4">                                    
                    {{ Form::select('banco', $bcs, @$c->banco ? $c->banco : old('banco') , ['id' => 'banco', 'class'=>'form-control']) }}

                    @if ($errors->has('banco'))
                    <div class="help-block">
                        <strong>{{ $errors->first('banco') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
        </div>        
        <div class="form-group">
            <div class="{{ $errors->has('agencia') ? 'has-error' : '' }}">
                {{Form::label('agencia', 'Agencia', ['class' => 'col-md-2 control-label']) }}
                <div class="col-md-2">                                    
                    {{Form::text('agencia', @$c->agencia ? $c->agencia : old('agencia'), ['class' => 'form-control'])}}

                    @if ($errors->has('agencia'))
                    <div class="help-block">
                        <strong>{{ $errors->first('agencia') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="{{ $errors->has('conta') ? 'has-error' : '' }}">
                {{Form::label('conta', 'Conta', ['class' => 'col-md-1 control-label']) }}
                <div class="col-md-3">                                    
                    {{Form::text('conta', @$c->conta ? $c->conta : old('conta'), ['class' => 'form-control'])}}
                    @if ($errors->has('conta'))
                    <div class="help-block">
                        <strong>{{ $errors->first('conta') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="{{ $errors->has('operacao') ? 'has-error' : '' }}">
                {{Form::label('operacao', 'Operacao', ['class' => 'col-md-1 control-label']) }}
                <div class="col-md-3">                                    
                    {{Form::text('operacao', @$c->operacao ? $c->operacao : old('operacao'), ['class' => 'form-control'])}}
                    @if ($errors->has('operacao'))
                    <div class="help-block">
                        <strong>{{ $errors->first('operacao') }}</strong>
                    </div>
                    @endif
                </div>
            </div>
        </div>        
        <div>
            <h4>Observação</h4>
        </div> 
        <div class="form-group{{ $errors->has('observacao') ? ' has-error' : '' }}">
            <label class="col-md-2 control-label">Max <span id="counter">200</span> caracteres</label>
            <div class="col-md-10">
                <textarea name="observacao" id="textarea-maxlength" class="form-control contar" maxlength="200" rows="4">{{@$c->observacao}}</textarea>

                @if ($errors->has('observacao'))
                <span class="help-block">
                    <strong>{{ $errors->first('observacao') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div>
            <h4>Status</h4>
        </div> 
        <div class="form-group{{ $errors->has('condicao') ? ' has-error' : '' }}">
            <label class="col-md-2 control-label text-right">Ativo</label>
            <div class="col-md-8">

                <label class="radio-inline">
                    <input type="radio" name="condicao" value="1" checked="checked"/>Sim
                </label>
                <label class="radio-inline">
                    <input type="radio" name="condicao" value="0"/>Não
                </label>
            </div>
        </div> 
        <div class="form-group text-right container">
            <button type="submit" class="btn btn-primary">
                Enviar
            </button>
        </div>

    </form>

</div>
@endsection
