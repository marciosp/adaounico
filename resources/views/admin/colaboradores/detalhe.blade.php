@extends('layouts.disable')
@section('content')

<?php
    
?>
<ul class="list-unstyled">
    <li>
        <b>Dados Pessoais</b>
        <ul>            
            <li>CPF: {!! Format::documentMask($c->cpf,'###.###.###-##') !!}</li>
            <li>RG: {{$c->rg}}</li>
            <li>Cargo: {{$c->cargos->cargo}}</li>
            <li>Telefone: {!! Format::documentMask($c->telefone, '(##)#########') !!}</li>
        </ul>
    </li>
    <li>
        <b>Endereco</b>
        <ul>
            <li>CEP: {!! Format::documentMask($c->cep,'#####-###')!!}</li>
            <li>Endereço: {{$c->endereco}}</li>
            <li>Número: {{$c->numero}}</li>
            <li>Complemento: {{$c->complemento}}</li>
            <li>Bairro: {{$c->bairro}}</li>
            <li>Cidade: {{$c->cidade}}</li>
            <li>Estado: {!! @$estados[$c->estado] !!}</li>
        </ul>   
    <li>
        <b>Dados Bancários</b>
        <ul>
            <li>Banco: {{$c->banco}}</li>
            <li>Agencia: {{$c->agencia}}</li>
            <li>Conta: {{$c->conta}}</li>
            <li>Operação: {{$c->operacao}}</li>
        </ul>      
    </li>
    <b>Observação</b>
    <ul>
        <li>{{$c->observacao}}</li>
    </ul>
    <li>
    <b>Status: </b>{!! $status[$c->status] !!}
    </li>
</ul>
@endsection

