@extends('layouts.app')

@section('content')
<div class="container colaboradores">
    @if(empty($colaboradores))
    <div class="alert alert-danger">
        Você não tem nenhum colaborador cadastrado.
    </div>

    @else
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h2>Listagem de Colaboradores</h2>
        </div>
        <div class="col-md-6 col-xs-12">
            @include('admin.search')
        </div>
    </div>
    <div class="row text-right">
        <div class="col-md-12 margin-bottom">
            <a class="btn btn-default" href="colaborador" role="button">Cadastrar</a>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12 col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>Status</th>

                        <th colspan="3" class="text-center">Ações</th>
                    </tr>
                    @foreach ($colaboradores as $c)       

                    <tr>
                        <td> {{$c->nome}} </td>
                        <td> {!! Format::documentMask($c->telefone, '(##)#########') !!} </td>
                        <td class="text-center"> {!! $status[$c->status] !!} </td>
                        <td class="text-center">
                            <a class="cursor-pointer emodal" alt="{{$c->nome}}" href="colaborador/mostra/{{$c->id}}">
                                <i class="fas fa-search"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="colaborador/{{$c->id}}">
                                <i class="far fa-edit"></i>
                            </a>
                        </td>        
                        <td class="text-center">
                            <a href="colaborador/remove/{{$c->id}}" onclick="return confirm('Deseja apagar?')">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach 
                </table>

            </div>
        </div>
    </div>
</div>
<div class="row text-center">
    {{ $colaboradores->links() }}
</div>
</div>

@endif
</div>
@endsection
