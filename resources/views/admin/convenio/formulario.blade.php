@extends('layouts.app')
@section('content')
<div class="container">
    <h2>{{ $item ?'Editar': 'Cadastrar' }} Convênio</h2>
    <div class="row text-right">
        <a class="btn btn-default" href="/admin/convenio/lista" role="button">Lista Completa</a>
    </div>


    <form action="{{url('admin/convenio/salvar')}}" method="post" class="form-horizontal" role="form" novalidate>
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{@$item->id}}"/>


        <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
            <label class="col-md-2 control-label">Nome*</label>
            <div class="col-md-6">
                <input id="nome" type="text" class="form-control" name="nome" value="{{@$item->nome ? $item->nome : old('nome')}}"  required autofocus/>

                @if ($errors->has('nome'))
                <span class="help-block">
                    <strong>{{ $errors->first('nome') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label class="col-md-2 control-label text-right">Ativo</label>
            <div class="col-md-8">

                <label class="radio-inline">
                    <input type="radio" name="status" value="1" checked="checked"/>Sim
                </label>
                <label class="radio-inline">
                    <input type="radio" name="status" value="0"/>Não
                </label>
            </div>
        </div> 
        <div class="form-group text-right container">
            <button type="submit" class="btn btn-primary">
                Enviar
            </button>
        </div>

    </form>

</div>
@endsection
