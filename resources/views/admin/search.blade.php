{!! Form::open(array('rout' => 'admin.search', 'class'=>'form searchform margin-bottom')) !!}
   <div class="input-group">
       {!! Form::text('search', null,
                           array('',
                            'style' => 'height: 28px',
                                'class'=>'form-control',
                                'placeholder'=>'Buscar...')) !!}
   <span class="input-group-btn">
        <button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>
   </span>
</div>
  {!! Form::close() !!}
  
