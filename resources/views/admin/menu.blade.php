<nav id="menu-horizontal" class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        </div>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown ">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cadastros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ url('/admin/funcoes/lista') }}">Funções</a></li>
            <li><a href="{{ url('/admin/cursos/lista') }}">Cursos</a></li>
            <li><a href="{{ url('/admin/cargos/lista') }}">Cargos</a></li>
          </ul>
        </li>
        <li class="dropdown ">
          <a href="{{url('/admin/membros/lista')}}" >Membros</a>
          <ul class="dropdown-menu">
            <li><a href="{{url('/admin/colaboradores')}}">Funcionarios</a></li>
            <li><a href="{{url('/admin/cargos')}}">Cargos</a></li>
          </ul>
        </li>
      </ul>
    </div>
</nav>

