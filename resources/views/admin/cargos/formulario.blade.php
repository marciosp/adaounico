@extends('layouts.app')
@section('content')
<div class="categorias container">
    <h2>{{ $item ? 'Editar': 'Cadastrar' }} Cargos</h2>
    <div class="row text-right">
    <a class="btn btn-default" href="{{ $item ? '../': '' }}lista" role="button">Lista Completa</a>
   </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel-group">
                <div class=" panel-default">
                    <div class="panel-body">   
                        <form action="{{ $item ? '../': '' }}salvar" method="post" class="form-horizontal" role="form" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{@$item->id}}"/>
                                
                            <div class="form-group{{ $errors->has('v') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Cargo</label>
                                <div class="col-md-8">
                                    <input id="cargo" type="text" class="form-control" name="cargo" value="{{@$item->cargo ? $item->cargo : old('cargo')}}"  required autofocus/>
                                </div>
                                @if ($errors->has('cargo'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('cargo') }}</strong>
                                </span>
                                @endif
                            </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">
                       {{ $item ? 'Editar': 'Cadastrar' }}
                    </button>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection