@extends('layouts.app')
@section('content')
<div class="container" id="membros">
    <h2>{{ $m ?'Editar': 'Cadastrar' }} Membros</h2>
    <div class="row text-right">
        <a class="btn btn-default" href="{{ $m ? '../': '' }}lista" role="button">Lista Completa</a>
    </div>


    <form action="{{ $m ? '../': '' }}salvar" method="post" class="form-horizontal" role="form" novalidate enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{@$m->id}}"/>
        <section class="dadospessoais">
            <div>
                <h4>Dados Pessoais</h4>
            </div>
            <div class="col-md-9">
                <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                    <label class="col-md-2 control-label">Nome*</label>
                    <div class="col-md-10">
                        <input id="nome" type="text" class="form-control" name="nome" value="{{@$m->nome ? $m->nome : old('nome')}}"  required autofocus/>

                        @if ($errors->has('nome'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="{{ $errors->has('rg') ? 'has-error' : '' }}">
                        {{Form::label('rg', 'RG', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('rg', @$m->rg ? $m->rg : old('rg'), ['class' => 'form-control'])}}

                            @if ($errors->has('rg'))
                            <div class="help-block">
                                <strong>{{ $errors->first('rg') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="{{ $errors->has('cpf') ? 'has-error' : '' }}">
                        {{Form::label('cpf', 'CPF', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('cpf', @$m->cpf ? $m->cpf : old('cpf'), ['class' => 'form-control cpf','placeholder' => '000.000.000-00'])}}
                            @if ($errors->has('cpf'))
                            <div class="help-block">
                                <strong>{{ $errors->first('cpf') }}</strong>
                            </div>
                            @endif
                        </div>

                    </div>
                </div>
            
                <div class="form-group">
                    <div class="{{ $errors->has('telefone') ? 'has-error' : '' }}">
                        {{Form::label('telefone', 'Telefone', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('telefone', @$m->telefone ? $m->telefone : old('telefone'), ['class' => 'form-control telefone','placeholder' => '(00)00000000'])}}

                            @if ($errors->has('telefone'))
                            <div class="help-block">
                                <strong>{{ $errors->first('telefone') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="{{ $errors->has('celular') ? 'has-error' : '' }}">
                        {{Form::label('celular', 'Celular', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('celular', @$m->celular ? $m->celular : old('celular'), ['class' => 'form-control telefone','placeholder' => '(00)000000000'])}}

                            @if ($errors->has('celular'))
                            <div class="help-block">
                                <strong>{{ $errors->first('celular') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                 <div class="form-group">
                    <div class="{{ $errors->has('estado_civil') ? 'has-error' : '' }}">
                        {{Form::label('estado_civil', 'Estado Civil', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">    
                            {{Form::select('estado_civil', ['solteiro'=>'Solteiro','casado'=>'Casado','viuvo'=> 'Viuvo'], @$m->estado_civil ? $m->estado_civil : 'casado', ['class'=>'form-control'])}}                           
                            @if ($errors->has('estado_civil'))
                            <div class="help-block">
                                <strong>{{ $errors->first('estado_civil') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>                
                    <div class="{{ $errors->has('dtnasc') ? 'has-error' : '' }}">
                        {{Form::label('dtnasc', 'Nascimento', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('dtnasc', @$m->dtnasc ? $m->dtnasc : old('dtnasc'), ['class' => 'form-control date','placeholder' => 'dia/mes/ano'])}}

                            @if ($errors->has('dtnasc'))
                            <div class="help-block">
                                <strong>{{ $errors->first('dtnasc') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>                
                <div class="form-group">
                    <div class="{{ $errors->has('naturalidade') ? 'has-error' : '' }}">
                        {{Form::label('naturalidade', 'Naturalidade', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('naturalidade', @$m->naturalidade ? $m->naturalidade : old('naturalidade'), ['class' => 'form-control naturalidade'])}}

                            @if ($errors->has('naturalidade'))
                            <div class="help-block">
                                <strong>{{ $errors->first('naturalidade') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="{{ $errors->has('profissao') ? 'has-error' : '' }}">
                        {{Form::label('profissao', 'Profissão', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('profissao', @$m->profissao ? $m->profissao : old('profissao'), ['class' => 'form-control profissao'])}}

                            @if ($errors->has('profissao'))
                            <div class="help-block">
                                <strong>{{ $errors->first('profissao') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>    
               <div class="form-group">
                    <div class="{{ $errors->has('email') ? 'has-error' : '' }}">
                        {{Form::label('email', 'E-mail', ['class' => 'col-md-2 control-label']) }}
                        <div class="col-md-4">                                    
                            {{Form::text('email', @$m->email ? $m->email : old('email'), ['class' => 'form-control email'])}}

                            @if ($errors->has('email'))
                            <div class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>
                    {{Form::label('escolaridade', 'Escolaridade', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                      
                            {{Form::select('escolaridade', 
                            ['fundamental'=>'Fundamental','medio'=>'Médio','Superior'=> 'Superior','posgraduacao'=> 'Pós-graduação','mestrado'=> 'Mestrado','doutorado'=> 'Doutorado' ], 
                            @$m->escolaridade ? $m->escolaridade : old('escolaridade'), 
                            ['class'=>'form-control'])
                            }}                           

                    
                    </div>
                </div>                   
            </div>                
            <div class="col-sm-6 col-md-3">
              <div class="thumbnail">
                @if(@$m->id)
                    <div style="height: 240px;text-align: center;background:url({{ URL::to('/') }}/upload/membros/{{$m->id}}/{{$m->foto}}) no-repeat top center;background-size:cover" id="blah">      
                @else
                    <div style="height: 240px;text-align: center;" id="blah">
                @endif

                               
                </div>
                <div class="caption">
                  <p>
                    <label class="btn btn-primary btn-file" style="width:100%">
                        Enviar Foto <input type="file"  id="imgInp" class="hide" name="foto">
                    </label>
                  </p>
                </div>
              </div>
            </div>                       
        </section>
        <section class="row">            
            <div>
                <h4>Filiação</h4>
            </div>            
            <div class="form-group">
                <div class="{{ $errors->has('nome_pai') ? 'has-error' : '' }}">
                    {{Form::label('nome_pai', 'Nome do pai', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('nome_pai', @$m->nome_pai ? $m->nome_pai : old('nome_pai'), ['class' => 'form-control nome_pai'])}}

                        @if ($errors->has('nome_pai'))
                        <div class="help-block">
                            <strong>{{ $errors->first('nome_pai') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="{{ $errors->has('nome_mae') ? 'has-error' : '' }}">
                    {{Form::label('nome_mae', 'Nome da mãe', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('nome_mae', @$m->nome_mae ? $m->nome_mae : old('nome_mae'), ['class' => 'form-control nome_mae'])}}

                        @if ($errors->has('nome_mae'))
                        <div class="help-block">
                            <strong>{{ $errors->first('nome_mae') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>            
        </section>
        <section class="row">            
            <div>
                <h4>Filhos           
                <a class="cursor-pointer">
                    <i class="fas fa-plus-circle add-filho"></i>
                    <i class="fas fa-minus-circle remove-filho"></i>
                </a>
                </h4>         
            </div>            
            <div class="form-group filhos">

                    {{Form::label('nome_filho', 'Nome', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">     
                        {{Form::text('filho[0][nome]', @$m->id ? @$m->filhos()->first()->nome : '', ['class' => 'form-control nome'])}}
                    </div>      
                    {{Form::label('dtnasc_filho', 'Data nascimento', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('filho[0][dtnasc]',  @$m->id ? @$m->filhos()->first()->dtnasc : '', ['class' => 'form-control date','placeholder' => 'dia/mes/ano'])}}
                    </div>

            </div>
                            
          @if (@$m->id && @$m->filhos()->count() > 1)
           <?php
                $i = 1;
                $filhos = $m->filhos->slice(1);
           ?>

                @foreach($filhos as $filho)

                <div class="form-group filhos">
                    {{Form::label('nome_filho', 'Nome', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">     
                    <input class="form-control" name="filho[<?= $i ?>][nome]" type="text" value="<?= $filho->nome ?>" >
                    </div>      
                    {{Form::label('dtnasc_filho', 'Data nascimento', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">    
                    <input class="form-control" name="filho[<?= $i ?>][dtnasc]" type="text" value="<?= $filho->dtnasc ?>" placeholder="dia/mes/ano" >
                    </div>
                </div>
                <?php ++$i ?>
                @endforeach
            @endif

            
            <script>
                $(".add-filho").click(function(){
                        var filhoTemplate = $(".filhos:first").clone();
                        var len = $(".filhos").length;
                        filhoTemplate.find("input:text").val('');
                        filhoTemplate.find('.nome').attr('name','filho['+len+'][nome]');
                        filhoTemplate.find('.date').mask('00/00/0000').attr('name','filho['+len+'][dtnasc]');
                        $(".filhos:last").after(filhoTemplate);
                    })
                    $(".remove-filho").click(function(){
                        var len = $(".filhos").length;
                        $(".filhos:last").remove();
                    })    
                </script>


        </section>        
        <section>
            <div>
                <h4>Endereco</h4>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('cep') ? 'has-error' : '' }}">
                    {{Form::label('cep', 'CEP', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('cep', @$m->cep ? $m->cep : old('cep'), ['class' => 'form-control cep','placeholder' => '00000-000'])}}
                        @if ($errors->has('cep'))
                        <div class="help-block">
                            <strong>{{ $errors->first('cep') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <button type="button" class="btn btn-default btn-xs cep-busca" onclick="buscaCep()">Preencher</button>
                </div>
            </div>
            <div class="form-group">

                <div class="{{ $errors->has('endereco') ? 'has-error' : '' }}">
                    {{Form::label('endereco', 'Endereco', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-7">                                    
                        {{Form::text('endereco', @$m->endereco ? $m->endereco : old('endereco'), ['class' => 'form-control','id'=>'endereco'])}}

                        @if ($errors->has('endereco'))
                        <div class="help-block">
                            <strong>{{ $errors->first('endereco') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="{{ $errors->has('numero') ? 'has-error' : '' }}">
                    {{Form::label('numero', 'Numero', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-2">                                    
                        {{Form::text('numero', @$m->numero ? $m->numero : old('numero'), ['class' => 'form-control number','id' => 'numero'])}}
                        @if ($errors->has('numero'))
                        <div class="help-block">
                            <strong>{{ $errors->first('numero') }}</strong>
                        </div>
                        @endif
                    </div>

                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('complemento') ? 'has-error' : '' }}">
                    {{Form::label('complemento', 'Complemento', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('complemento', @$m->complemento ? $m->complemento : old('complemento'), ['class' => 'form-control','id' => 'complemento'])}}

                        @if ($errors->has('complemento'))
                        <div class="help-block">
                            <strong>{{ $errors->first('complemento') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="{{ $errors->has('bairro') ? 'has-error' : '' }}">
                    {{Form::label('bairro', 'Bairro', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('bairro', @$m->bairro ? $m->bairro : old('bairro'), ['class' => 'form-control','id'=>'bairro'])}}
                        @if ($errors->has('bairro'))
                        <div class="help-block">
                            <strong>{{ $errors->first('bairro') }}</strong>
                        </div>
                        @endif
                    </div>

                </div>
            </div>
            <div class="form-group">
                <div class="{{ $errors->has('cidade') ? 'has-error' : '' }}">
                    {{Form::label('cidade', 'Cidade', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('cidade', @$m->cidade ? $m->cidade : old('cidade'), ['class' => 'form-control','id'=>'cidade'])}}

                        @if ($errors->has('cidade'))
                        <div class="help-block">
                            <strong>{{ $errors->first('cidade') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="{{ $errors->has('estado') ? 'has-error' : '' }}">
                    {{Form::label('estado', 'Estado', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{ Form::select('estado', array_merge(['' => 'Selecione o estado'],$uf), @$m->estado ? $m->estado : old('estado') , ['id' => 'uf', 'class'=>'form-control']) }}  
                        @if ($errors->has('estado'))
                        <div class="help-block">
                            <strong>{{ $errors->first('estado') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div>
                <h4>Histórico Eclesiástico</h4>
            </div>       
            <div class="form-group">
                    {{Form::label('dt_batismo', 'Data de Filiação', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('dt_filiacao', @$m->dt_filiacao ? $m->dt_filiacao : old('dt_filiacao'), ['class' => 'form-control date','id'=>'dt_filiacao','placeholder' => 'dia/mes/ano'])}}
                    </div>
            </div>            
            <div class="form-group">
                
                <label class="col-md-2 control-label text-right">Batizado nas aguas</label>                
                <div class="col-md-2">
                    <label class="radio-inline">
                        {{Form::radio('batismo', '1', @$m->batismo == '1' ? true : '')}}Sim
                    </label>
                    <label class="radio-inline">
                    {{Form::radio('batismo', '0', @$m->batismo == '0' ? true : '')}}Não
                    </label>
                </div>                
                    {{Form::label('dt_batismo', 'Data do Batismo', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{Form::text('dt_batismo', @$m->dt_batismo ? $m->dt_batismo : old('dt_batismo'), ['class' => 'form-control date','id'=>'dt_batismo','placeholder' => 'dia/mes/ano'])}}
                    </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label text-right">Batizado no espirito</label>
                <div class="col-md-8">
                    <label class="radio-inline">
                        {{Form::radio('batismo_espirito', '1', @$m->batismo_espirito == '1' ? true : '')}}Sim
                    </label>
                    <label class="radio-inline">
                        {{Form::radio('batismo_espirito', '0', @$m->batismo_espirito == '0' ? true : '')}}Não
                    </label>
                </div>
            </div>            
            
            <div class="form-group">
               
                    {{Form::label('tempo_fe', 'Tempo de Fé', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-2">
                        <div class="col-md-8">
                            <input class="form-control number" name="tempo_fe[ano]" type="text" value="{{@intdiv($m->tempo_fe,12)}}">       
                        </div>       
                            <label for="anos" class="col-md-4 control-label ">anos</label>          
                    </div>

                    

                                     
                    <div class="col-md-2">
                        <div class="col-md-8">
                            <input class="form-control" name="tempo_fe[mes]" type="text" value="{{@$m->tempo_fe%12}}">
                        </div>         
                        <label for="meses" class="col-md-4 control-label">meses</label>        
                    </div>                            
                
            </div>
            <div class="form-group">
                    {{Form::label('funcoes_id', 'Função no ministerio', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                   
                
                    {{ Form::select('funcoes_id', $fs->union(['' => 'Nenhuma função']), 
                    @$m->funcoes_id ? $m->funcoes_id : old('funcoes_id'), ['id' => 'funcoes_id', 'class'=>'form-control']) }}  

                    </div>
                
                    {{Form::label('cursos_id', 'Curso Teológico', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-4">                                    
                        {{ Form::select('cursos_id', $crs->union(['' => 'Nenhum curso']), 
                        @$m->cursos_id ? $m->cursos_id : old('cursos_id'), 
                        ['id' => 'cursos_id', 'class'=>'form-control']) }}  

                    </div>
            </div>
            </section>
            <section>
            <div>
                <h4>Consagração</h4>
            </div> 

            <div class="form-group">

                    {{Form::label('cargos_id', 'Cargo', ['class' => 'col-md-2 control-label']) }}
                    <div class="col-md-2">                                    
                        {{ Form::select('cargos_id', $cs->union(['0' => 'Nenhum cargo']), 
                        @$m->cargos_id ? $m->cargos_id : old('cargos_id'), 
                        ['id' => 'cargos_id', 'class'=>'form-control']) }}  
                    </div>

                    {{Form::label('dt_consagrado', 'Data', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-3">                                    
                        {{Form::text('dt_consagrado', @$m->dt_consagrado ? $m->dt_consagrado : old('dt_consagrado'), ['class' => 'form-control date','id' => 'dt_consagrado','placeholder' => 'dia/mes/ano'])}}
                    </div>
               
                    {{Form::label('local_consagrado', 'Local', ['class' => 'col-md-1 control-label']) }}
                    <div class="col-md-3">                                    
                        {{Form::text('local_consagrado', @$m->local_consagrado ? $m->local_consagrado : old('local_consagrado'), ['class' => 'form-control','id'=>'local_consagrado'])}}
                    </div>
            </div>
        </section>
        <section>
            <div>
                <h4>Situação do Membro</h4>
            </div> 
            <label class="col-md-2 control-label text-right"></label>
            <div class="col-md-8">

                <label class="radio-inline">
                {{Form::radio('status', '1', @$m->status == '1' ? true : '')}}Ativo
                </label>
                <label class="radio-inline">
                {{Form::radio('status', '0', @$m->status == '0' ? true : '')}}Inativo
                </label>
            </div>
        </section>
        <div class="form-group text-right container">
            <button type="submit" class="btn btn-primary">
                Enviar
            </button>
        </div>

    </form>

</div>
@endsection
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script>
$(document).ready(function() {
    function readURL(input) {
        if (input.files && input.files[0]) {            
            var reader = new FileReader();
            reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    var width = this.width;  
                    var cssStyle = '';
                    if(width > 240) {
                        cssStyle = {'background': 'url(' + e.target.result + ') no-repeat center center','background-size':'cover'};
                    } else {
                        cssStyle = {'background': 'url(' + e.target.result + ') no-repeat center center'};
                    }
                    $('#blah').css(cssStyle);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imgInp").change(function(){    
        readURL(this);
    });
});
</script>
