@extends('layouts.disable')
@section('content')
<ul class="list-unstyled">
    <div class="row">
        <div class="col-md-7">
            <li>
                <b>Dados Pessoais</b>
                <ul>            
                    <li>RG: {{@$m->rg}}</li>            
                    <li>CPF: {{ $m->cpf ? Format::documentMask($m->cpf,'###.###.###-##') : ''}}</li>            
                    <li>Telefone: {{ $m->telefone ? Format::documentMask($m->telefone, '(##)#########') : ''}}</li>
                    <li>Celular: {{ $m->celular ? Format::documentMask($m->celular, '(##)#########') : ''}}</li>
                    <li>Estado Civil: {{ucfirst($m->estado_civil)}}</li>
                    <li>Nascimento: {{@$m->dtnasc}}</li>
                    <li>Naturalidade: {{@$m->naturalidade}}</li>
                    <li>Profissão: {{@$m->profissao}}</li>
                    <li>E-mail: {{@$m->email}}</li>
                    <li>Escolaridade: {{@$m->escolaridade}}</li>
                </ul>
            </li>
        </div>
        <div class="col-md-5">
            <div style="height:230px;background:url({{ URL::to('/') }}/upload/membros/{{$m->id}}/{{$m->foto}}) no-repeat top center;background-size:cover">        
            </div>
        </div>
    </div>
    <div class="row">
        <li>
            <b>Filiação</b>
            <ul>            
                <li>Nome do pai: {{ucwords($m->nome_pai)}}</li>
                <li>Nome da mãe: {{ucwords($m->nome_mae)}}</li>            
            </ul>
        </li>
    </div>
    @if(!empty($filhos->first()))
    <div class="row">
        <li>
            <b>Filhos</b>
            <ul>     
            @foreach($filhos as $filho)
                <li>Nome: {{$filho->nome}} - Nascido em {{$filho->dtnasc}}</li>                
            @endforeach
            </ul>
        </li>
    </div>
    @endif
    <div class="row">
        <li>
            <b>Endereco</b>
            <ul>
                <li>CEP: {{$m->cep ? Format::documentMask($m->cep,'#####-###') : ''}}</li>
                <li>Endereço: {{$m->endereco}}</li>
                <li>Número: {{$m->numero}}</li>
                <li>Complemento: {{$m->complemento}}</li>
                <li>Bairro: {{$m->bairro}}</li>
                <li>Cidade: {{$m->cidade}}</li>
                <li>Estado: {!! @$estados[$m->estado] !!}</li>
            </ul>   
        <li>
    </div>
    <div class="row">
        <b>Histórico Eclesiástico</b>
        <ul>
            <li>Data Filiação: {{$m->dt_filiacao}}</li>
            <li>Batizado nas águas: {!! @$bool[$m->batismo] !!}</li>
            <li>Data do Batismo: {{$m->dt_batismo}}</li>
            <li>Batizado no espírito: {!! @$bool[$m->batismo_espirito] !!}</li>
            <li>Tempo de Fé: {{@intdiv($m->tempo_fe,12)}} anos e {{@$m->tempo_fe%12}} meses</li>
            <li>Função no ministério: {{!empty($m->funcoes->funcao) ? $m->funcoes->funcao : ''}}</li>
            <li>Curso Teológico: {{!empty($m->cursos->curso) ? $m->cursos->curso : ''}}</li>
        </ul> 
    </div>
    <div class="row">
        <b>Consagração</b>
        <ul>
            <li>Cargo: {{!empty($m->cargos->cargo) ? $m->cargos->cargo : ''}}</li>
            <li>Data: {{$m->dt_consagrado}}</li>
            <li>Local: {{$m->local_consagrado}}</li>
        </ul> 
    </div>
    
    
    
    
    <li>
        <b>Situação do membro: </b>{!! @$status[$m->status] !!}
    </li>
</ul>
@endsection

