@extends('layouts.app')

@section('content')

<section class="container listagem">

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h2>Membros<br><small>Total de {{$total}} membros cadastrados</small></h2>
        </div>
        <div class="col-md-6 col-xs-12">
            @include('admin.search')
        </div>        
    </div>


    <div class="row text-right">
        <div class="col-md-12 margin-bottom">
            <a class="btn btn-default" href="cadastrar" role="button">Cadastrar</a>
        </div>
    </div>
    @if(count($itens) == 0)
    <div class="alert alert-danger">
        Você não tem nenhum membro cadastrado.
    </div>

    @else
    <div class="row">

        <div class="col-md-12 col-xs-12">
            @foreach ($itens as $m)       
            <div class="col-md-3">
                <div class="thumbnail">    
                    <div style="height:230px;background:url({{ URL::to('/') }}/upload/membros/{{$m->id}}/{{$m->foto}}) no-repeat center center;background-size:cover">                        
                    </div>
                    <div class="caption text-center">
                        <h5><b>{{ucwords($m->nome)}}</b></h5>
                        <div class="row" >
                            <p class="col-md-4" style="padding: 0;padding-right: 3px;">
                                <a href="mostra/{{$m->id}}" class="btn btn-primary btn-block emodal" alt="{{ucwords($m->nome)}}" role="button">Ver</a> 
                            </p>
                            <p class="col-md-4" style="padding: 0;padding-right: 3px;">
                                <a href="editar/{{$m->id}}" class="btn btn-default btn-block" role="button">Editar</a>
                            </p>
                            <p class="col-md-4" style="padding: 0;padding-right: 3px;">
                                <a href="remove/{{$m->id}}" class="btn btn-danger btn-block" role="button" onclick="return confirm('Deseja apagar?')">Excluir</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach 
        </div>
    </div>
</div>

<div class="row text-center">
    {{ $itens->links() }}
</div>

@endif
</section>
@endsection
<script>

</script>