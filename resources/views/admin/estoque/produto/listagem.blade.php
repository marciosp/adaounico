@extends('layouts.app')

@section('content')
<section class="container">
    @if(empty($produtos))
    <div class="alert alert-danger">
        Você não tem nenhum produto cadastrado.
    </div>

    @else
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h2>Listagem de Produtos</h2>
        </div>
        <div class="col-md-6 col-xs-12">
            @include('admin.search')
        </div>
    </div>
    <div class="row text-right">
        <div class="col-md-12 margin-bottom">
            <a class="btn btn-default" href="produto" role="button">Cadastrar</a>
        </div>
    </div>
    <div class="row">

        <div class="col-md-10 col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>Produto</th>
                        <th>Quantidade</th>
                        <th>Preço</th>
                        <th colspan="3" class="text-center">Ações</th>
                    </tr>
                    @foreach ($produtos as $p)       
                    <tr>
                        <td> {{$p->nome}} </td>
                        <td> {{$p->quantidade}} </td>
                        <td> R$ {{ number_format($p->preco, 2) }} </td>
                        <td class="text-center">
                            <a class="cursor-pointer emodal" data-size="md" alt="Produto: {{$p->nome}}" href="produto/mostra/{{$p->id}}">
                                <i class="fas fa-search"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="produto/{{$p->id}}">
                                <i class="far fa-edit"></i>
                            </a>
                        </td>        
                        <td class="text-center">
                            <a href="produto/remove/{{$p->id}}" onclick="return confirm('Deseja apagar?')">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach 
                </table>

            </div>
        </div>
    </div>
    <div class="row text-center">
        {{ $produtos->links() }}
    </div>
@endif
</section>
@endsection
