@extends('layouts.disable')
@section('content')
<ul>
    <li>
        <b>Descricao:</b> {{ $p->descricao }}
    </li>
    <li>
        <b>Quantidade:</b> {{ $p->quantidade }}
    </li>
    <li>
        <b>Preço:</b> R$ {{ number_format($p->preco, 2) }}
    </li>
</ul>
@endsection

