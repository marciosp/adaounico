@extends('layouts.app')
@section('content')
<div class="categorias container">
    <h2>{{ $p ?'Editar': 'Cadastrar' }} Produto</h2>
    <div class="row text-right">
        <a class="btn btn-default" href="/admin/produtos" role="button">Lista Completa</a>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel-group">
                <form action="/admin/produto/save" method="post" class="form-horizontal" role="form" novalidate>
                    <div class=" panel-default">
                        <div class="panel-body">   
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{@$p->id}}"/>
                            <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Nome*</label>
                                <div class="col-md-8">
                                    <input id="nome" type="text" class="form-control" name="nome" value="{{@$p->nome ? $p->nome : old('nome')}}"  required autofocus/>
                                </div>
                                @if ($errors->has('nome'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('nome') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('categoria_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Categoria*</label>
                                <div class="col-md-8">                                    
                                    {{ Form::select('categoria_id', $cs, @$p->categoria_id ? $p->categoria_id : old('categoria_id') , ['id' => 'categoria_id', 'class'=>'form-control']) }}
                                </div>
                                @if ($errors->has('categoria_id'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('categoria_id') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('quantidade') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Quantidade*</label>
                                <div class="col-md-8">
                                    <input id="quantidade" type="text" class="form-control" name="quantidade" value="{{@$p->quantidade}}"  required autofocus/>
                                </div>
                                @if ($errors->has('quantidade'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('quantidade') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('preco') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Valor*</label>
                                <div class="col-md-8">
                                    <input id="preco" type="text" class="form-control money2" name="preco" value="{{@$p->preco}}"  required autofocus/>
                                </div>
                                @if ($errors->has('preco'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('preco') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('descricao') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Descricao* <br> (max 200 caracteres)</label>
                                <div class="col-md-8">
                                    <textarea name="descricao" id="descricao" class="form-control" rows="4">{{@$p->descricao}}</textarea>
                                </div>
                                @if ($errors->has('descricao'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('descricao') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">
                            Cadastrar
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection