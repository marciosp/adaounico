@extends('layouts.app')
@section('content')
<div class="categorias container">
    <h2>{{ $categoria ?'Editar': 'Cadastrar' }} Categoria</h2>
    <div class="row text-right">
    <a class="btn btn-default" href="/admin/categorias" role="button">Lista Completa</a>
   </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel-group">
                <div class=" panel-default">
                    <div class="panel-body">   
                        <form action="/admin/categoria/save" method="post" class="form-horizontal" role="form" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{@$categoria->id}}"/>
                                
                            <div class="form-group{{ $errors->has('categoria') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Categoria</label>
                                <div class="col-md-8">
                                    <input id="categoria" type="text" class="form-control" name="categoria" value="{{@$categoria->categoria ? $categoria->categoria : old('categoria')}}"  required autofocus/>
                                </div>
                                @if ($errors->has('categoria'))
                                <span class="help-block col-md-8 col-md-6 col-md-offset-4">
                                    <strong>{{ $errors->first('categoria') }}</strong>
                                </span>
                                @endif
                            </div>
                    </div>

                    <div class="form-group{{ $errors->has('condicao') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label text-right">Ativo</label>
                        <div class="col-md-8">

                            <label class="radio-inline">
                                <input type="radio" name="condicao" value="1" checked="checked"/>Sim
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="condicao" value="0"/>Não
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">
                        Cadastrar
                    </button>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
</div>

</div>

<?php /*

  <h1>{{ $categoria ?'Editar': 'Novo' }} produto</h1>
  @if ($errors->any())
  <div class="alert alert-danger">
  <ul>
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
  </ul>
  </div>
  @endif
  <form action="@if($produto) /produtos/atualiza/{{$produto->id}} @else /produtos/adiciona @endif" method="post">
  {{ csrf_field() }}
  <div class="form-group">
  <label>Nome</label>
  <input name="nome" class="form-control" value="{{@$produto->nome}}"/>
  </div>
  <div class="form-group">
  <label>Categoria</label>
  <select name="categoria_id" class="form-control">
  @foreach ($categorias as $c)
  <option value="{{$c->id}}">{{$c->nome}}</option>
  @endforeach
  </select>
  </div>
  <div class="form-group">
  <label>Descricao</label>
  <input name="descricao" class="form-control" value="{{@$produto->descricao}}"/>
  </div>
  <div class="form-group">
  <label>Valor</label>
  <input name="valor" type="text" class="form-control" value="{{@$produto->valor}}"/>
  </div>
  <div class="form-group">
  <label>Quantidade</label>
  <input type="number" name="quantidade" class="form-control" value="{{@$produto->quantidade}}"/>
  </div>
  <div class="form-group">
  <label>Tamanho</label>
  <input name="tamanho" class="form-control" />
  </div>
  <button type="submit"
  class="btn btn-primary btn-block">Submit</button>
  </form>
 */ ?>
@endsection