@extends('layouts.app')

@section('content')
<section class="container">
    @if(empty($categorias))
    <div class="alert alert-danger">
        Você não tem nenhum produto cadastrado.
    </div>

    @else
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <h2>Listagem de categorias</h2>
        </div>
        <div class="col-md-6 col-xs-12">
            @include('admin.search')
        </div>
    </div>
    <div class="row text-right">
        <div class="col-md-12 margin-bottom">
            <a class="btn btn-default" href="categoria" role="button">Cadastrar</a>
        </div>
    </div>
    <div class="row">

        <div class="col-md-10 col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>Categoria</th>
                        <th class="text-center">Status</th>
                        <th colspan="3" class="text-center">Ações</th>
                    </tr>
                    @foreach ($categorias as $c)       

                    <tr>
                        <td> {{$c->categoria}} </td>
                        <td class="text-center"> {!! $status[$c->status] !!} </td>




                        <td class="text-center">
                            <a class="cursor-pointer emodal" alt="Categoria: {{$c->categoria}}" href="categoria/mostra/{{$c->id}}">
                                <i class="fas fa-search"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="categoria/{{$c->id}}">
                                <i class="far fa-edit"></i>
                            </a>
                        </td>        
                        <td class="text-center">
                            <a href="categoria/remove/{{$c->id}}" onclick="return confirm('Deseja apagar?')">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach 
                </table>

            </div>
        </div>
    </div>
    <div class="row text-center">
        {{ $categorias->links() }}
    </div>
</div>

@endif
</section>
@endsection
