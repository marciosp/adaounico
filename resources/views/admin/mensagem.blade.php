    @if (session('status'))
    <div class="row">
        <div class="col-md-6">
                <div class="alert alert-{{session('status')}} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                    </button>
                <strong>{{session('msg')}}</strong>
            </div>
        </div>
    </div>
    @endif